# coding: utf-8
import sys


alphabet = ''
# Определим границы для алфавита
# print ord(u'А'), ord(u'Я'), ord(u'а'), ord(u'я')
for i in range(ord(u'А'), ord(u'я')+1):
	alphabet += unichr(i)

shift = 5
# Приведем к нормированному виду
shift %= len(alphabet)
cesar = ''
for ind, x in enumerate(alphabet):
	if (ind+shift) < len(alphabet):
		cesar += alphabet[ind+shift]
	else:
		cesar += alphabet[ind+shift-len(alphabet)]

print u'Введите кодируемую фразу'
#raw = raw_input().decode('utf-8')
# Возьмем текущую локаль консоли
raw = raw_input().decode(sys.stdout.encoding)
print u'Ввели слово', raw
cipher = u''
for x in raw:
	if x not in alphabet:
		cipher += x
		continue
	for ind, y in enumerate(alphabet):
		if y == x:
			cipher += cesar[ind]
			break

print u'Зашифровали', cipher

decrypted = u''
for x in cipher:
	if x not in cesar:
		decrypted += x
		continue
	for ind, y in enumerate(cesar):
		if y == x:
			decrypted += alphabet[ind]
			break

print u'Расшифровали', decrypted

