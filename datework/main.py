# coding: utf-8
import datetime

t_now = datetime.datetime.now()
# Выведем все атрибуты и методы объекта datetime
print dir(t_now)

if t_now.second % 2 == 0:
	print u'Четная секунда: %s' % t_now.strftime('%d.%m.%Y %H:%M:%S')
else:
	print u'Нечетная секунда: %s' % t_now.strftime('%d.%m.%Y %H:%M:%S')

